## Mutliplication table exercises

This is a group of exercises to help practice coding in different languages.

### Expected output
If your output does not match the expectation exactly, it is wrong.
```
   0  1  2  3  4  5  6  7
1 02 03 04 05 06 07 08 09
2 04 06 08 10 12 14 16 18
3 06 09 12 15 18 21 24 27
4 08 12 16 20 24 28 32 36
5 10 15 20 25 30 35 40 45
6 12 18 24 30 36 42 48 54
7 14 21 28 35 42 49 56 63
8 16 24 32 40 48 56 64 72
9 18 27 36 45 54 63 72 81
```

### Currently done languages
- PHP
- C
- Go
- Python
- Bash

### Planned languages
- Vala
- C++
- Rust
- Ruby
