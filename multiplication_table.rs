fn main() {
    let _rows = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    let _columns = [2, 3, 4, 5, 6, 7, 8, 9];

    print!("  ");
    for x in _columns.iter() {
        print!(" {:2}", x);
    }
    println!("");

    for y in _rows.iter() {
        print!("{:2}", y);
        for x in _columns.iter() {
            print!(" {:02}", y*x);
        }
        println!("");
    }
}
