#!/usr/bin/env bash

declare -a rows=(1 2 3 4 5 6 7 8 9)
declare -a columns=(2 3 4 5 6 7 8 9)

printf "  "
for x in ${columns[@]}
do
	printf " %2d" $x
done
printf "\n"

for y in ${rows[@]}
do
	printf "%2d" $y
	for x in ${columns[@]}
	do
		printf " %02d" $(( $y * $x ))
	done
	printf "\n"
done
