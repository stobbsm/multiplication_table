#!/usr/bin/env python

rows = [1, 2, 3, 4, 5, 6, 7, 8, 9]
columns = [2, 3, 4, 5, 6, 7, 8, 9]

print("  "),
for x in columns:
    print('{:2d}'.format(x)),

print('')

for y in rows:
    print('{:2d}'.format(y)),
    for x in columns:
        cell = y * x
        print('{:02d}'.format(cell)),

    print('')
