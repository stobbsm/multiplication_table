package main

import "fmt"

func main() {
	columns := []int{2, 3, 4, 5, 6, 7, 8, 9}
	rows := []int{1, 2, 3, 4, 5, 6, 7, 8, 9}

	// Print first line
	fmt.Printf(" ")
	for x := range(columns) {
		fmt.Printf(" %2d", x)
	}
	fmt.Printf("\n")
	for y := range(rows) {
		fmt.Printf("%d", rows[y])
		for x := range(columns) {
			cell := rows[y] * columns[x]
			fmt.Printf(" %02d", cell) 
		}
		fmt.Printf("\n")
	}
}
