#include <stdio.h>

void main() {
	int rows[9];
	int columns[8];
	int i,y,x;

	// Initialize arrays
	for (i=0; i<9; i++) {
		rows[i]=i+1;
	}

	printf("  ");
	for (i=0; i<8; i++) {
		columns[i]=i+2;
		printf(" %2d", columns[i]);
	}
	printf("\n");

	for(y=0; y<sizeof(rows)/sizeof(int); y++) {
		printf("%2d", rows[y]);
		for(x=0; x<sizeof(columns)/sizeof(int); x++) {
			printf(" %02d", rows[y] * columns[x]);
		}
		printf("\n");
	}
}
