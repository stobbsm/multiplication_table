<?php

$rows = array(1, 2, 3, 4, 5, 6, 7, 8, 9);
$columns = array(2, 3, 4, 5, 6, 7, 8, 9);

printf("  "); // Empty cell at header
foreach ($columns as $x) {
	printf(" %2d", $x);
}
printf("\n");
foreach ($rows as $y) {
	printf("%2d", $y);
	foreach ($columns as $x) {
		$cell = $y * $x;
		printf(" %02d", $cell);
	}
	printf("\n");
}
